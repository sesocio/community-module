// Routes
import ReferralRoutes from './src/modules/referral/routes';
import GiftCardRoutes from './src/modules/giftcard/routes';
import RankingRoutes from './src/modules/ranking/routes';
import AcademyRoutes from './src/modules/academy/routes'

// Stores
import ReferralStore from './src/modules/referral/store';
import GiftCardStore from './src/modules/giftcard/store';
import RankingStore from './src/modules/ranking/store';
import AcademyStore from './src/modules/academy/store'

export {
	AcademyRoutes,
  AcademyStore,
  ReferralRoutes,
  ReferralStore,
  GiftCardRoutes,
  GiftCardStore,
  RankingRoutes,
  RankingStore
};