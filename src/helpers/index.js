export function hasOwnNestedProperty(obj, propertyPath) {
  if(!propertyPath) return false;

  let properties = propertyPath.split('.');

  for(let i = 0; i < properties.length; i++) {
    let prop = properties[i];

    if(!obj || !obj.hasOwnProperty(prop)) {
      return false;
    } else {
      obj = obj[prop];
    }
  }

  return true;
};

export function getRankingCategory(category) {
  switch(category) {
    case 'ballena':
      return 'whale';
      break;

    case 'tiburon':
      return 'shark';
      break;

    case 'pez_espada':
      return 'swordfish';
      break;

    case 'pejerrey':
      return 'silverfish';
      break;

    case 'mojarra':
      return 'mojarra';
      break;
  }
}

export function getShareLink(method, url, text) {
  let link;
	switch (method) {
    case 'facebook':
      return `https://www.facebook.com/sharer/sharer.php?u=${encodeURIComponent(url + '&s=f')}`;
      break;

    case 'linkedin':
      return `https://www.linkedin.com/sharing/share-offsite/?url=${encodeURIComponent(url + '&s=l')}`;
      break;

    case 'twitter':
      link = `https://twitter.com/share?text=${text}&via=sesociocom`;
      if(url) link += `&url=${encodeURIComponent(url + '&s=t')}`;
      return  link;
      break;

    case 'whatsapp':
      link = `https://api.whatsapp.com/send?text=${text}`;
      if(url) link += ` ${encodeURIComponent(url + '&s=ww')}`;
      return  link;
      break;

    case 'whatsapp-mobile':
      link = `whatsapp://send?text=${text}`;
      if(url) link += ` ${encodeURIComponent(url + '&s=wa')}`;
      return  link;
      break;

    case 'telegram':
      link = `https://t.me/share/url?text=${text}`;
      if(url) link += `&url=${encodeURIComponent(url + '&s=tl')}`;
      return  link;
      break;
  }
}

export function getLazyUserAvatar() {
  return 'data:image/jpeg;base64,/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAAA8AAD/4QMraHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjMtYzAxMSA2Ni4xNDU2NjEsIDIwMTIvMDIvMDYtMTQ6NTY6MjcgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDUzYgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkU2OTNERjVBMjE4NTExRUM4OUE4Q0VDNkI0RDZDRDA3IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkU2OTNERjVCMjE4NTExRUM4OUE4Q0VDNkI0RDZDRDA3Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6RTY5M0RGNTgyMTg1MTFFQzg5QThDRUM2QjRENkNEMDciIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6RTY5M0RGNTkyMTg1MTFFQzg5QThDRUM2QjRENkNEMDciLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7/7gAOQWRvYmUAZMAAAAAB/9sAhAAGBAQEBQQGBQUGCQYFBgkLCAYGCAsMCgoLCgoMEAwMDAwMDBAMDg8QDw4MExMUFBMTHBsbGxwfHx8fHx8fHx8fAQcHBw0MDRgQEBgaFREVGh8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx//wAARCABBAEADAREAAhEBAxEB/8QAlwAAAQQDAQAAAAAAAAAAAAAABQACBAYDBwgBAQACAwEBAAAAAAAAAAAAAAAABAIDBQEGEAACAgECBAIIBQQDAAAAAAABAgMEBQARIUESBjETUWFxgSIyogeRoWJyFLEVFgjBQlIRAAEEAAIGCAUFAQAAAAAAAAEAEQIDITFBUbESBAXwYXGBkSIyE6HB0VJi8UJykhQV/9oADAMBAAIRAxEAPwDqnQhQcrmaOLiR7LMZJT0Vq0al5pn236I0HFj+Q57DUJ2COarstjAY/qh6R91ZICSWVMJWbiK8YSe1t+uRt4kPqVW9uq2nL8R8VUBbPPyDxP0WVe1KBG1mzdtn0zWph9MbRr+Wu+yNJJ713/PHSZHvKTdqUANq1m7UP/qG1MfpkaRfy0eyNBI70f546DId5WJ4+6saDJFKmbrLxNeQLBaA/RIu0Tn1Mq+3XGnH8h8Vwi2GXnHgfpsRDE5mjlIneszCSI9FitIpSaF9t+iSM8VP5HluNWQsEslbXbGYw/RTtTVihZjKxYyi1h0M0rMsVasm3XNM52SNN+bH8BxPAahZPdDqu2wQD9Co2GwsleR8jkXWzmrI2mnHyRJ4iCuD8sa/ix+JuPhGutsTjLpgFCqpvNLGZ6MOramd393YLtHt+1ns5Y/j0Ki7tsOp3c8EjjX/ALO54KP+NWEsmAHXK3dP+1P3Iylx2wK1+38cCRDF5SWrLLyMrygxg+pU4ek6XlfqTMKNazdpf7V/cDGXE/yOKDPY5iBKI40q2kXmY2TaJj+llG/pGuRv1rsqBoXVHa/c+F7owVTOYWwLOOup1xSDgQQdmR1PFXRgVYHwOmQXSpDJuZwsliRMjjnWtmqw2hnPySp4mCwB80bfip+JePjXZW+Iwl0wKXtqfzRwmOjHq2KTh8rFk6K2EQwyqzRWaz7dcMyHZ4325qfxHEcDqVc94Op1WCYfoFAiT+490TTyDethlENYcjanTqlf2pEyoP3NqA803+3aqwN+x9ENpz+COauTC5Z/3Hz1ls/2z2+z9NGOCXItHvweZn8lCRzKKr7fuOqLiWV9ADrQVSKe7OtejFJcsOdkhro0rkn0KgY6V3Sm3RfLdm90YnI0sVex0q5i/CLEOKiUzWljdiqebHH1eWz9JIU8vHbXdxAK6A/1Ukz2FzvdvZOXhatLS/jXnpsyt5M0q9Mg+Asvxp5ZPHlpml2SvEMujNXpdA5U/t3dEM8Y2rZlTDZHIWoE6on9rxKyH9q6pPlm/wB21LEblj6J7Rl8E/tMK2MltD5rdu1M/vndR9KjRTk+snau8N6X1k7UZ1cmFWO4+3sdLlEzk1CvPaihWuLrxo88cYct0Izg7Alz4aV4gHPQn+DMTh+5D8XDPXrL5sVWC23V5pox+VGV6j0bbgPv0bdW/PflpZ00Ia060txYZZMcIEvv0gSzq3SVDDqDtHtIfg36ePj6tAKJRRPAYCjDlp83HSgit2oRA91VUTyxhuoCRl23Ckc9M8PE56EnxUo5fuVi00kkG7sCrjIrR+apbqzJ7p0U/Sx1Tdk+ojal+J9L6iNqXaZVMXJWB3apatQv7RO7D6WGin0tqJ2o4b0tqJ2ozq5MLFbrrZrSQMdhIu2/oPI6jOO8GU6p7khLUqfkqFuWtLS/kzY61w6Ldfp61KsCGXzFdHU7bMpHEcNZzGJYrXJEg8ck/FY61HWhpixNemQbSXLBUyOSSS8hRUUePAAcBw10RMiwXDIQDlW6vCsMCRL4IAN9aEYsGWROW8SVk1JRQbuwq+LjrE7NbtVYU9pnRj9KnVN3pbWRtS/E+ltZG1MruMd3PPWc9NfMKLFU8AP5EKhJk9rRqjj2N6NA8s2+7agHdsI0SxHaM0cJABJOwHEk6uTCqeR+5eArM8dUS3nU7dUQAjJ9Tttv7t9ZlvNao4ReS1auUWyxk0e3PwQKv31Hl8o0N6OKgsY3p9T7hwfmDOwVerhw0pHmAtm0mjqWh/z/AGYPEmT5rJN3/XxOQWtTiTII69Vt0k6fLI+VVbZlY8eOuy5iKpNEb2tQPLTdF5Hd1YZo3jvuR2/akSKfzKUjkKDMo8sE8Bu6kge/TdXNKpFi8e1I28ptiHDS7PorVrSWWgdhxke54KyHqr4dTYtHgR/ImQpCntWNnc+1fTqk+abfbtS5O9YBojie05KdmsVHk6XkGQwTxss1SyoBaGaPikig+O3McxuOep2Q3gystr3w2nR1FV7Mdyyt21lq1xBVzdaHy7FYb9LeafLWaAn54n6uB5eB4jSXFXkUyfCTbcMEzyqXuXxhINIFyOzSOpawAAAA4AcANeZXtk2SOORemRQw9B0EOgEhJI4416UUKPQNADIJdesoZSp8CNj79BQCtqY7uaT/AB7EwU4xazdyBVgrHfpXy/gaacj5IlI4nn4Dideo4e96otjIjo68RzOXt3ShHGROA+Z6kdwuKjxlLyBIZ55Gaa3ZYANNNJxeRgPDfkOQ2HLTdcN0Ml6q9wNp09ZU7U1Yhue7dxWcqiC/F1FCTDMh6ZY2PDdHHh6+R56qupjYGkFExxEgTGQyIzHTwWuct9uu4qLs1PpydYblShWKcD9SNsjH9p92sS/lUhjAuFrcPzyyGF0d/wDKOB745f1Pcq/PQydc7WKFqFvQ8En9VUjSMuFtGcStKPO+EOc93+UZD5JQUMnYO1ehamb0JBJ/VlA0R4W05RKJc74QZT3v4xkfkrBift13FedWudOMrHYsXKyzkfpRd0U/uJ9mnqOVSOMywWbxHPLJhqY7n5SxPdHL+x7lsbBdu4rB1TBQiKl9jNM56pZCPAu58fUPActbdVMaw0QskRxMiTKRzJzKJatUktCEtCEtCEtCEtCEtCEtCEtCF//Z';
}