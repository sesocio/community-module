import actions from "./actions";
import mutations from "./mutations";

const state = {
  ranking: [],
  total: 0,
  currentPage: 1,
  loadingNextPage: false,
  userScore: {}
};

export default {
  namespaced: true,
  state,
  actions,
  mutations
};