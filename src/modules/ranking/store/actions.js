import Axios from 'axios';
import { hasOwnNestedProperty } from '../../../helpers';

const getUserScore = ({ commit }) => {
  return new Promise(async (resolve, reject) => {
    try {
      let url = `${process.env.VUE_APP_API_BASE_URL}/api/v2/social_network_rankings/get_current_user_position`;

      const response = await Axios.get(url).then(response => {
        if(response.status === 200 && hasOwnNestedProperty(response, 'data.data.attributes')) {
          commit('SET_USER_SCORE', response.data.data.attributes);
        }
      });

      resolve(response);

    } catch (e) {
      reject(e)
    }
  });
}

const getRanking = ({ commit, state }, newPage) => {
  return new Promise(async (resolve, reject) => {
    try {
      let page = newPage ? state.currentPage : 1,
          pageSize = 20;

      commit('SET_LOADING_NEW_PAGE', true);

      let url = `${process.env.VUE_APP_API_BASE_URL}/api/v2/social_network_rankings?page[number]=${page}&page[size]=${pageSize}&sort=ranking&include=user`;

      const response = await Axios.get(url).then(response => {
        if(response.status === 200 && hasOwnNestedProperty(response, 'data.data')) {
          commit('SET_RANKING', response.data.data);
          commit('SET_LOADING_NEW_PAGE', false);
        }
      });

      resolve(response);

    } catch (e) {
      reject(e)
    }
  });
};

const getRankingCount = ({ commit }) => {
  return new Promise(async (resolve, reject) => {
    try {
      let url = `${process.env.VUE_APP_API_BASE_URL}/api/v2/social_network_rankings?page[size]=0&stats[total]=count`;

      const response = await Axios.get(url).then(response => {
        if(response.status === 200 && hasOwnNestedProperty(response, 'data.meta.stats.total.count')) {
          commit('SET_RANKING_COUNT', response.data.meta.stats.total.count);
        }
      });

      resolve(response);

    } catch (e) {
      reject(e)
    }
  });
}

export default {
  getUserScore,
  getRankingCount,
  getRanking
};