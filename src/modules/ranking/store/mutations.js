const SET_RANKING = (state, payload) => {
  state.ranking = state.ranking.concat(payload);
  state.currentPage = state.currentPage + 1;
};

const SET_RANKING_COUNT = (state, payload) => {
  state.total = payload;
};

const SET_USER_SCORE = (state, payload) => {
  state.userScore = payload;
};

const SET_LOADING_NEW_PAGE = (state, payload) => {
  state.loadingNextPage = payload;
};

export default {
  SET_RANKING,
  SET_RANKING_COUNT,
  SET_USER_SCORE,
  SET_LOADING_NEW_PAGE
};