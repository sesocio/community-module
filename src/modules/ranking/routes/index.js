export default [
  {
    path: "/ranking",
    name: "ranking",
    component: () => import(/* webpackChunkName: "Index" */ '../pages/Index'),
    title: "ranking",
    meta: { requiresAuth: true }
  }
];