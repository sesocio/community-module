import Axios from 'axios';

const items = [
  {
    image: 'https://cdn.vuetifyjs.com/images/cards/sunshine.jpg',
    title: 'Capitulo 1',
    description: 'Top western road trips',
    link: '/dashboard',
    externalLink: false,
    new: true,
  },

  {
    image: 'https://cdn.vuetifyjs.com/images/cards/sunshine.jpg',
    title: 'Cripto',
    description: 'Conocé todo lo que necesitas saber sobre las criptomonedas y llevá tus inversiones en Criptos a otro nivel.',
    link: '/dashboard',
    externalLink: false,
    chapters: 4,
    new: true
  },

  {
    image: 'https://cdn.vuetifyjs.com/images/cards/sunshine.jpg',
    title: 'Top western road trips',
    description: 'Top western road trips',
    link: 'https://www.sesocio.com',
    externalLink: true,
    chapters: 1,
  },

  {
    image: 'https://cdn.vuetifyjs.com/images/cards/sunshine.jpg',
    title: 'Top western road trips',
    description: 'Top western road trips',
    link: 'https://www.sesocio.com',
    externalLink: true,
    chapters: 1,
  },

  {
    image: 'https://cdn.vuetifyjs.com/images/cards/sunshine.jpg',
    title: 'Top western road trips',
    description: 'Top western road trips',
    link: 'https://www.sesocio.com',
    externalLink: true,
    chapters: 1,
  }
];

const gridExample = {
  title: "titulo de la guia",
  description: "descripcion de la guia",
  items: items
};

const podcasts = [
  {
    image: require('../../../assets/images/academy/podcasts/episode_1.jpg'),
    title: "Episodio 1",
    description: "Fer Carolei en este primer episodio de \"La libertad de tus finanzas\" conversa a solas con Vero Silva acerca de su experiencia creando Apprendo.",
    link: "https://open.spotify.com/episode/1uK1R8cbA7CrWlkk1m6OZr?si=9c0f659885fa4e74",
    externalLink: true
  },
  {
    image: require('../../../assets/images/academy/podcasts/episode_2.jpg'),
    title: "Episodio 2",
    description: "Guido Quaranta y la intimidad de la tarjeta SeSocio, primer tarjeta 100% transparente de Argentina.",
    link: "https://open.spotify.com/episode/5ASyqn4OeCTxDHx3uiHK3D?si=08724b265a434dce",
    externalLink: true
  },
  {
    image: require('../../../assets/images/academy/podcasts/episode_3.jpg'),
    title: "Episodio 3",
    description: "Christian Petersen y la cocina de un gran emprendedor. Consejos, fama, logros, fracasos y mucho más.",
    link: "https://open.spotify.com/episode/273ov1P4SaXCmMQWEpC2BP?si=a2041430e3994245",
    externalLink: true
  },
  {
    image: require('../../../assets/images/academy/podcasts/episode_4.jpg'),
    title: "Episodio 4",
    description: "Marcela Pagano nos cuenta cómo invertir en Argentina y qué factores tener en cuenta en un contexto de elecciones.",
    link: "https://open.spotify.com/episode/3hYirKszJjpIbgHN6OSBtE?si=e81983eff7844a06",
    externalLink: true
  },
  {
    image: require('../../../assets/images/academy/podcasts/episode_5.jpg'),
    title: "Episodio 5",
    description: "Jorge Bellsolá conversa con Fer Carolei sobre el proyecto \"Seamos Bosques\" y la acción llevada en conjunto con SeSocio. ¿Hay casos de éxitos en el cambio climático?",
    link: "https://open.spotify.com/episode/09t0b0OjovBN8EIxrT49Cq?si=daa2ee3032594239",
    externalLink: true
  },
  {
    image: require('../../../assets/images/academy/podcasts/episode_6.jpg'),
    title: "Episodio 6",
    description: "Walter Queijeiro, periodista deportivo, nos cuenta ¿Como ahorrar para invertir? ¿Por donde comenzar? asociado al mundo del futbol.",
    link: "https://open.spotify.com/episode/6wUc83BGr6nYXYyxF8bkLW?si=ea2987c7961242b5",
    externalLink: true
  }
];

const getNews = ({ commit }) => {
  return new Promise(async (resolve, reject) => {
    try {
      commit('CLEAR_NEWS');

      let url = `${process.env.VUE_APP_API_COMMUNITY_URL}/sscool/sscool/v1/article/list?type=cafe_crypto&limit=3`;

      await Axios.get(url).then(response => {
        if(response.status === 200) {
          commit('SET_NEWS', response.data.articles);
        }
      });

      resolve();

    } catch (e) {
      reject(e)
    }
  });
};

const getGuides = ({ commit }) => {
  return new Promise(async (resolve, reject) => {
    try {
      commit('CLEAR_GUIDES');
      //let url = `${process.env.VUE_APP_API_COMMUNITY_URL}/gift-cards/gift-cards/list_giftcard`;

      //await Axios.get(url).then(response => {
        //if(response.status === 200) {
          //commit('SET_GIFTCARDS', response.data);
        //}
      //});
      commit('SET_GUIDES', items);

      resolve();

    } catch (e) {
      reject(e)
    }
  });
};

const getCryptos = ({ commit }) => {
  return new Promise(async (resolve, reject) => {
    try {
      commit('CLEAR_CRYPTOS');
      //let url = `${process.env.VUE_APP_API_COMMUNITY_URL}/gift-cards/gift-cards/list_giftcard`;

      //await Axios.get(url).then(response => {
        //if(response.status === 200) {
          //commit('SET_GIFTCARDS', response.data);
        //}
      //});
      commit('SET_CRYPTOS', items);

      resolve();

    } catch (e) {
      reject(e)
    }
  });
};

const getPodcasts = ({ commit }) => {
  return new Promise(async (resolve, reject) => {
    try {
      commit('CLEAR_PODCASTS');
      //let url = `${process.env.VUE_APP_API_COMMUNITY_URL}/gift-cards/gift-cards/list_giftcard`;

      //await Axios.get(url).then(response => {
        //if(response.status === 200) {
          //commit('SET_GIFTCARDS', response.data);
        //}
      //});
      commit('SET_PODCASTS', podcasts);

      resolve();

    } catch (e) {
      reject(e)
    }
  });
};

const getGrid = ({ commit }, grid) => {
  return new Promise(async (resolve, reject) => {
    try {
      commit('CLEAR_GRID');
      //let url = `${process.env.VUE_APP_API_COMMUNITY_URL}/gift-cards/gift-cards/list_giftcard`;

      //await Axios.get(url).then(response => {
        //if(response.status === 200) {
          //commit('SET_GIFTCARDS', response.data);
        //}
      //});
      commit('SET_GRID', gridExample);

      resolve();

    } catch (e) {
      reject(e)
    }
  });
};

const getArticle = ({ commit }, article) => {
  return new Promise(async (resolve, reject) => {
    try {
      commit('CLEAR_ARTICLE');
      //let url = `${process.env.VUE_APP_API_COMMUNITY_URL}/gift-cards/gift-cards/list_giftcard`;

      //await Axios.get(url).then(response => {
        //if(response.status === 200) {
          //commit('SET_GIFTCARDS', response.data);
        //}
      //});
      commit('SET_ARTICLE', items);

      resolve();

    } catch (e) {
      reject(e)
    }
  });
};

const searchArticles = ({ commit }, search) => {
  return new Promise(async (resolve, reject) => {
    try {
      commit('CLEAR_ARTICLES_SEARCH');
      let limit = 10;
      let url = `${process.env.VUE_APP_API_BASE_URL}/${process.env.VUE_APP_API_VERSION_URL}/articles/get_articles_academy/${limit}/0?type=&name=${search}&category=&all=`;

      await Axios.get(url).then(response => {
        if(response.status === 200) {
          commit('SET_ARTICLES_SEARCH', response.data);
        }
      });

      resolve();

    } catch (e) {
      reject(e)
    }
  });
};

const clearSearchArticles = ({ commit }) => {
  commit('CLEAR_ARTICLES_SEARCH');
}

const sendComment = ({ commit }, comment) => {
  return new Promise(async (resolve, reject) => {
    try {
      let url = `${process.env.VUE_APP_API_COMMUNITY_URL}/sscool/sscool/v1/comment`;

      const response = await Axios.post(url, { comment: comment });

      resolve(response);
    } catch (e) {
      reject(e)
    }
  });
}

export default {
  sendComment,
  getNews,
  getGuides,
  getCryptos,
  getPodcasts,
  getGrid,
  getArticle,
  searchArticles,
  clearSearchArticles
};