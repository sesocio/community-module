const SET_NEWS = (state, payload) => {
  state.news = payload;
  state.newsLoaded = true;
};

const CLEAR_NEWS = (state) => {
  state.news = [];
  state.newsLoaded = false;
};

const SET_GUIDES = (state, payload) => {
  state.guides = payload;
  state.guidesLoaded = true;
};

const CLEAR_GUIDES = (state) => {
  state.guides = [];
  state.guidesLoaded = false;
};

const SET_CRYPTOS = (state, payload) => {
  state.cryptos = payload;
  state.cryptosLoaded = true;
};

const CLEAR_CRYPTOS = (state) => {
  state.cryptos = [];
  state.cryptosLoaded = false;
};

const SET_PODCASTS = (state, payload) => {
  state.podcasts = payload;
  state.podcastsLoaded = true;
};

const CLEAR_PODCASTS = (state) => {
  state.podcasts = [];
  state.podcastsLoaded = false;
};

const SET_GRID = (state, payload) => {
  state.grid = payload;
  state.gridLoaded = true;
};

const CLEAR_GRID = (state) => {
  state.grid = [];
  state.gridLoaded = false;
};

const SET_ARTICLE = (state, payload) => {
  state.article = payload;
  state.articleLoaded = true;
};

const CLEAR_ARTICLE = (state) => {
  state.article = [];
  state.articleLoaded = false;
};

const SET_ARTICLES_SEARCH = (state, payload) => {
  state.articlesSearch = payload;
};

const CLEAR_ARTICLES_SEARCH = (state, payload) => {
  state.articlesSearch = {};
};

export default {
  SET_NEWS,
  SET_GUIDES,
  SET_CRYPTOS,
  SET_PODCASTS,
  SET_GRID,
  SET_ARTICLE,
  CLEAR_NEWS,
  CLEAR_GUIDES,
  CLEAR_CRYPTOS,
  CLEAR_PODCASTS,
  CLEAR_GRID,
  CLEAR_ARTICLE,
  SET_ARTICLES_SEARCH,
  CLEAR_ARTICLES_SEARCH
};