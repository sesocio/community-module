import actions from "./actions";
import mutations from "./mutations";

const state = {
  news: [],
  guides: [],
  cryptos: [],
  podcasts: [],
  grid: {},
  article: {},
  newsLoaded: false,
  guidesLoaded: false,
  cryptosLoaded: false,
  podcastsLoaded: false,
  gridLoaded: false,
  articleLoaded: false,
  articlesSearch: {}
};

export default {
  namespaced: true,
  state,
  actions,
  mutations
};