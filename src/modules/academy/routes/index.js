export default [
  {
    path: "/s-cool",
    name: "academy",
    component: () => import(/* webpackChunkName: "Index" */ '../pages/Index'),
    title: "academy"
  },
  /*
  {
    path: "/s-cool_beta/:section",
    name: "academy_section",
    component: () => import( webpackChunkName: "Grid" '../pages/Grid'),
    title: "academy_section",
  },
  */
  {
    path: "/s-cool/:section/articulos",
    name: "academy_grid",
    component: () => import(/* webpackChunkName: "Grid" */ '../pages/Grid'),
    title: "academy_grid",
    props: true
  },
  /*
  {
    path: "/s-cool/:section/:article_id",
    name: "academy_article",
    component: () => import(webpackChunkName: "Article" '../pages/Article'),
    title: "academy_article",
  },
  */
];

/*

/novedades
/novedades/articulo-1

/guias
/guias/guias-1
/guias/guias-1/articulo-1

/cripto
/cripto/mastersocio
/cripto/mastersocio/articulo-1

/podcasts

*/