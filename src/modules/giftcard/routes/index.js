export default [
  {
    path: "/giftcard",
    name: "giftcard",
    component: () => import(/* webpackChunkName: "Index" */ '../pages/Index'),
    title: "giftcard"
  },
  {
    path: "/giftcard/mis-giftcards",
    name: "giftcards",
    component: () => import(/* webpackChunkName: "MyGiftCards" */ '../pages/MyGiftCards'),
    title: "giftcards",
    meta: { requiresAuth: true }
  },
  {
    path: "/giftcard/regalar",
    name: "give_giftcard",
    component: () => import(/* webpackChunkName: "GiveGiftCard" */ '../pages/GiveGiftCard'),
    title: "give_giftcard",
    meta: { requiresAuth: true }
  }
];