const SET_GIFTCARDS = (state, payload) => {
  state.giftcards = payload;
};

const SET_MESSAGE = (state, payload) => {
  state.message = payload;
};

const SET_THEME = (state, payload) => {
  state.theme = payload;
};

const SET_AMOUNT = (state, payload) => {
  state.amount = payload;
};

const SET_CURRENCY = (state, payload) => {
  state.currency = payload;
};

const NEXT_STEP = (state) => {
  state.step = state.step + 1;
};

const PREV_STEP = (state) => {
  state.step = state.step - 1;
};

const SET_STEP = (state, payload) => {
  state.step = payload;
};

const NEXT_CREATION_KEY = (state) => {
  state.creationKey = state.creationKey + 1;
};

export default {
	SET_GIFTCARDS,
	SET_MESSAGE,
	SET_THEME,
	SET_AMOUNT,
	SET_CURRENCY,
	NEXT_STEP,
	PREV_STEP,
	SET_STEP,
	NEXT_CREATION_KEY
};