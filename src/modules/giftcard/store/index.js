import actions from "./actions";
import mutations from "./mutations";

const state = {
  giftcards: {},
  step: 1,
  message: {},
  amount: 0,
  currency: {},
  theme: 'happy',
  creationKey: 1
};

export default {
  namespaced: true,
  state,
  actions,
  mutations
};