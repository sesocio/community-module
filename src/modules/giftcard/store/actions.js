import Axios from 'axios';

const getGiftCards = ({ commit }) => {
  return new Promise(async (resolve, reject) => {
    try {
      let url = `${process.env.VUE_APP_API_COMMUNITY_URL}/gift-cards/gift-cards/list_giftcard`;

      await Axios.get(url).then(response => {
        if(response.status === 200) {
          commit('SET_GIFTCARDS', response.data);
        }
      });

      resolve();

    } catch (e) {
      reject(e)
    }
  });
};

const validateUser = ({ commit }) => {
  return new Promise(async (resolve, reject) => {
    try {
      let url = `${process.env.VUE_APP_API_COMMUNITY_URL}/gift-cards/gift-cards/validation`;

      const response = await Axios.get(url).then(response => {
        if(response.status === 200) {
          return response.data;
        }
      });

      resolve(response);

    } catch (e) {
      reject(e)
    }
  });
};

const create = ({ commit, state }) => {
  return new Promise(async (resolve, reject) => {
    try {
      let url = `${process.env.VUE_APP_API_COMMUNITY_URL}/gift-cards/gift-cards`;
      let data = {
        amount: state.amount,
        currency: state.currency.name,
        motive: state.theme,
        firstname: state.message.firstname,
        lastname: state.message.lastname,
        message: state.message.message,
        email: state.message.email
      };

      const response = await Axios.post(url, data).then(response => {
        if(response.status === 200) {
          return response.data;
        }
      });

      resolve(response);

    } catch (e) {
      reject(e)
    }
  });
};

const redeemGiftCard = ({ commit }, payload) => {
  return new Promise(async (resolve, reject) => {
    try {
      let url = `${process.env.VUE_APP_API_COMMUNITY_URL}/gift-cards/gift-cards/receive_gift`;

      const response = await Axios.post(url, { id: payload }).then(response => {
        if(response.status === 200) {
          return response.data;
        }
      });

      resolve(response);

    } catch (e) {
      reject(e)
    }
  });
};

const setTheme = ({ commit }, payload) => {
  commit('SET_THEME', payload);
};

const setMessage = ({ commit }, payload) => {
  commit('SET_MESSAGE', payload);
};

const setAmount = ({ commit }, payload) => {
  commit('SET_AMOUNT', payload);
};

const setCurrency = ({ commit }, payload) => {
  commit('SET_CURRENCY', payload);
};

const nextStep = ({ commit }) => {
  commit('NEXT_STEP');
};

const prevStep = ({ commit }) => {
  commit('PREV_STEP');
};

const setStep = ({ commit }, payload) => {
  commit('SET_STEP', payload);
};

const nextCreationKey = ({ commit }) => {
  commit('NEXT_CREATION_KEY');
};

export default {
  create,
  getGiftCards,
  redeemGiftCard,
  setMessage,
  setTheme,
  setAmount,
  setCurrency,
  nextStep,
  prevStep,
  setStep,
  validateUser,
  nextCreationKey
};