import { mount, createLocalVue  } from "@vue/test-utils";
import Vuex from 'vuex'
import Vuetify from 'vuetify';
import Hero from "../components/Hero";
import HowItWorks from "../components/not_logged_in/HowItWorks";
import Requirements from "../components/not_logged_in/Requirements";

const localVue = createLocalVue();
const vuetify = new Vuetify();
const mocks = { $t: key => key };

localVue.use(Vuex);

describe("Hero component", () => {
  let userState = {
    isLoggedIn: false
  };

  let dialogMock = jest.fn();

  let store = new Vuex.Store({
    modules: {
      user: {
        state: {},
        actions: {},
        namespaced: true
      },
      onboarding_dialog: {
        state: {},
        actions: {
          openOnboardingDialog: dialogMock
        },
        namespaced: true
      }
    }
  });

  let wrapper = mount(Hero, {
    store,
    localVue,
    vuetify,
    mocks: mocks
  });

  it('has a title', () => {
    let title = wrapper.find('[data-testid="title"]');
    expect(title.text()).toMatch('title');
  });

  it('has a subtitle', () => {
    let button = wrapper.find('[data-testid="description"]');
    expect(button.text()).toMatch('description');
  });

  it('has a button with text "Empezá a referir"', () => {
    let button = wrapper.find('[data-testid="button"]');
    expect(button.text()).toMatch('action');
  });

  it('if user is logged in call method "openOnboardingDialog" when button is clicked', () => {
    let button = wrapper.find('[data-testid="button"]');
    button.trigger('click');
    expect(dialogMock).toHaveBeenCalled();
  });
});

describe("HowItWorks component", () => {
  let wrapper = mount(HowItWorks, { 
    localVue,
    vuetify,
    mocks: mocks
  });

  it('has a title', () => {
    let title = wrapper.find('[data-testid="title"]');
    expect(title.text()).toMatch('title');
  });

  it('has a list with 5 text items', () => {
    let list = wrapper.findAll('[data-testid="list-item"]');
    expect(list.length).toBe(5);
  });
});

describe("Requirements component", () => {
  let wrapper = mount(Requirements, { 
    localVue,
    vuetify,
    mocks: mocks
  });

  it('has a title', () => {
    let title = wrapper.find('[data-testid="title"]');
    expect(title.text()).toMatch('title');
  });

  it('has a second item', () => {
    let second = wrapper.find('[data-testid="second"]');
    expect(second.text()).toMatch('second');
  });

  it('has a third item', () => {
    let third = wrapper.find('[data-testid="third"]');
    expect(third.text()).toMatch('third');
  });
});