import actions from "./actions";
import mutations from "./mutations";

const state = {
  jackpot: 0,
  jackpotDate: '',
  jackpotCurrency: '',
  jackpotTickets: 0,
  tickets: 0,
  accumulated: 0,
  accumulatedCurrency: '',
  code: null,
  refererCode: null,
  referrals: [],
  referralsLoaded: false,
  count: 0,
  currentPage: 1,
  totalPages: 0
};

export default {
  namespaced: true,
  state,
  actions,
  mutations
};