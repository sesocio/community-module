import Axios from 'axios';

const getJackpot = ({ commit }) => {
  return new Promise(async (resolve, reject) => {
    try {
      let url = `${process.env.VUE_APP_API_COMMUNITY_URL}/coupons/jackpot/total_amount`;

      const response = await Axios.get(url).then(response => {
        if(response.status === 200) {
          commit('SET_JACKPOT', response.data);
        }
      });

      resolve(response);

    } catch (e) {
      reject(e)
    }
  })
};

const getUserAccumulated = ({ commit }) => {
  return new Promise(async (resolve, reject) => {
    try {
      let url = `${process.env.VUE_APP_API_COMMUNITY_URL}/coupons/user/total_amount`;

      const response = await Axios.get(url).then(response => {
        if(response.status === 200) {
          commit('SET_ACCUMULATED', response.data);
        }
      });

      resolve(response);

    } catch (e) {
      reject(e)
    }
  })
};

const getTickets = ({ commit }) => {
  return new Promise(async (resolve, reject) => {
    try {
      let url = `${process.env.VUE_APP_API_COMMUNITY_URL}/coupons/user/total_tickets`;

      const response = await Axios.get(url).then(response => {
        if(response.status === 200) {
          commit('SET_TICKETS', response.data.total);
        }
      });

      resolve(response);

    } catch (e) {
      reject(e)
    }
  })
};

const getUserReferrals = ({ commit }) => {
  return new Promise(async (resolve, reject) => {
    try {
      let url = `${process.env.VUE_APP_API_COMMUNITY_URL}/coupons/user/get_referrals`;
      
      const response = await Axios.get(url).then(response => {
        if(response.status === 200) {
          commit('SET_REFERRALS', response.data.referrals);
          commit('SET_REFERRALS_COUNT', response.data.count);
        }
      });

      resolve(response);

    } catch (e) {
      reject(e)
    }
  })
};

const sendSMS = ({}, username) => {
  return new Promise(async (resolve, reject) => {
    try {
      let url = `${process.env.VUE_APP_API_COMMUNITY_URL}/coupons/user/send_sms_referrer`;

      console.log('username', username)

      const response = await Axios.post(url, {
        alias: username
      });

      resolve(response);

    } catch (e) {
      reject(e)
    }
  })
};

const getPage = ({ commit }, page) => {
  return new Promise(async (resolve, reject) => {
    try {
      commit('SET_CURRENT_PAGE', page);

      resolve({});

    } catch (e) {
      reject(e)
    }
  })
};

const getRefererCode = ({ commit }) => {
  return new Promise(async (resolve, reject) => {
    try {
      let url = `${process.env.VUE_APP_API_COMMUNITY_URL}/coupons/user/get_code_referrer`;

      const response = await Axios.get(url).then(response => {
        if(response.status === 200) {
          commit('SET_REFERAL_CODE', response.data.code);
        }
      });

      resolve(response);

    } catch (e) {
      reject(e)
    }
  })
};

const assignReferer = ({ commit }, code) => {
  return new Promise(async (resolve, reject) => {
    try {
      let url = `${process.env.VUE_APP_API_COMMUNITY_URL}/coupons/user/find_referrer`;

      const response = await Axios.post(url, {
        code: code
      });

      resolve(response);

    } catch (e) {
      reject(e)
    }
  })
};

const setReferrerCode = ({ commit }, code) => {
  commit('SET_REFERRER_CODE', code);
};

const clearReferrerCode = ({ commit }) => {
  commit('CLEAR_REFERRER_CODE');
};

const clearReferrals = ({ commit }) => {
  commit('CLEAR_REFERRALS');
};

export default {
  getJackpot,
  getUserAccumulated,
  getUserReferrals,
  getRefererCode,
  getTickets,
  getPage,
  sendSMS,
  assignReferer,
  setReferrerCode,
  clearReferrerCode,
  clearReferrals
};